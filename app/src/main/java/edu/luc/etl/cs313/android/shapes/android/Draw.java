package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.BLACK); //added this line - color
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		//final int i = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(paint.getColor());
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(Style.STROKE);

		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for (Shape s: g.getShapes()) {
			s.accept(this);
		}

		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());

		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		//final Style i = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(paint.getStyle());
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		final Iterator<? extends Point> point = s.getPoints().iterator();

		final float[] points = new float[s.getPoints().size() * 4];
		int index = 0;
		final float[] first = new float[2];
		if (point.hasNext()) {
			Point p = point.next();
			points[index] = p.getX();
			first[0] = points[index];
			index++;
			points[index] = p.getY();
			first[1] = points[index];
			index++;
		}
		while (point.hasNext()) {
			Point p = point.next();
			points[index] = p.getX();
			index++;
			points[index] = p.getY();
			index++;
			points[index] = p.getX();
			index++;
			points[index] = p.getY();
			index++;
		}
		points[index] = first[0];
		index++;
		points[index] = first[1];
		index++;

		canvas.drawLines(points, paint);

		return null;
	}
}
