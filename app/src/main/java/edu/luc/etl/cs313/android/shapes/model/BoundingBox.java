package edu.luc.etl.cs313.android.shapes.model;
import java.util.*;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {

		final Iterator<? extends Shape> list = g.getShapes().iterator();
		
		int minX = Integer.MAX_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxX = 0;
		int maxY = 0;
		
		while (list.hasNext()){
			final Location loc = list.next().accept(this);
			int x = loc.getX();
			int y = loc.getY();
			int xPlusWidth = x;
			int yPlusHeight = y;
			
			if (loc.getShape() instanceof Rectangle) {
				xPlusWidth += ((Rectangle) loc.getShape()).getWidth();
				yPlusHeight += ((Rectangle) loc.getShape()).getHeight();
			}
			
			if (minX > x) {
				minX = x;
			} if (minY > y) {
				minY = y;
			} if (maxX < xPlusWidth) {
				maxX = xPlusWidth;
			} if (maxY < yPlusHeight) {
				maxY = yPlusHeight;
			}
		}
		return new Location(minX, minY, new Rectangle(maxX - minX, maxY - minY));
	}

	@Override
	public Location onLocation(final Location l) {
		final Location loc = l.getShape().accept(this);
		final int x = l.getX() + loc.getX();
		final int y = l.getY() + loc.getY();

		return new Location(x, y, loc.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, r);
	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {

		int minX = Integer.MAX_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxX = 0;
		int maxY = 0;

		for (Point p: s.getPoints()) {
			if (p.getX() < minX) {
				minX = p.getX();
			} if (p.getX() > maxX) {
				maxX = p.getX();
			} if (p.getY() < minY) {
				minY = p.getY();
			} if (p.getY() > maxY) {
				maxY = p.getY();
			}
		}

		return new Location(minX, minY, new Rectangle(maxX - minX, maxY - minY));
	}
}
